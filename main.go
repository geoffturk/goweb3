package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"math"
	"math/big"
	"net/http"
	"regexp"
	"strconv"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"

	accounts "gitlab.com/geoffturk/goweb3/accounts"
	registry "gitlab.com/geoffturk/goweb3/registry"
	token "gitlab.com/geoffturk/goweb3/token"
)

// toAddressKey - START
const RE_ADDRHEX = `^0[xX][a-fA-F0-9]{40}$`

func mergeKey(a []byte, b []byte) string {
	h := sha256.New()
	h.Write(a)
	h.Write(b)
	return hex.EncodeToString(h.Sum(nil))
}

func hexStringToBytes(s string) ([]byte, error) {
	a := make([]byte, 20)
	j := 2
	for i := 0; i < len(a); i++ {
		n, err := strconv.ParseInt(s[j:j+2], 16, 10)
		if err != nil {
			return nil, fmt.Errorf("hexStringToBytes: %s", err)
		}
		a[i] = byte(n)
		j += 2
	}
	return a, nil
}

func addressToBytes(s string) ([]byte, error) {
	matched, err := regexp.MatchString(RE_ADDRHEX, s)
	if err != nil {
		return nil, err
	}
	if !matched {
		return nil, errors.New("invalid address hex")
	}

	bytes, err := hexStringToBytes(s)
	if err != nil {
		return nil, fmt.Errorf("addressToBytes: %s", err)
	}

	return bytes, err
}

func toAddressKey(zeroExHex string, salt string) (string, error) {
	addressBytes, err := addressToBytes(zeroExHex)
	if err != nil {
		return "", fmt.Errorf("toAddressKey: %s", err)
	}
	return mergeKey(addressBytes, []byte(salt)), nil
}

// toAddressKey - END

func main() {
	// create connection to local node
	client, err := ethclient.Dial("http://localhost:63545")
	if err != nil {
		log.Fatal("node connection failed", err)
	}

	fmt.Println("node connection established")

	// instantiate the registry
	registryAddress := common.HexToAddress("0xea6225212005e86a4490018ded4bf37f3e772161")
	registryInstance, err := registry.NewRegistry(registryAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	// need to find an easier way to do this / kudos to:
	// https://stackoverflow.com/questions/19073769/how-do-you-convert-a-slice-into-an-array#21399657
	// https://stackoverflow.com/questions/27762562/how-to-compare-32byte-with-byte-in-golang
	acctReg := make([]byte, 32)
	var acctReg32 [32]byte
	acctReg = []byte("AccountRegistry")
	copy(acctReg32[:], acctReg[0:15])

	// get the accounts index address
	acctsIndexAddr, err := registryInstance.AddressOf(&bind.CallOpts{}, acctReg32)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Accounts Index Address:", acctsIndexAddr)

	// instantiate the accounts index
	acctsIndexInstance, err := accounts.NewAccounts(acctsIndexAddr, client)
	if err != nil {
		log.Fatal(err)
	}

	// get total number of accounts
	totalAccounts, err := acctsIndexInstance.EntryCount(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Total Accounts:", totalAccounts)

	// get the list of all accounts
	allAccounts := make([]common.Address, 0)
	// this was a brain twister (BigInts in a for loop declaration) / kudos to:
	// https://stackoverflow.com/questions/29930501/big-int-ranges-in-go
	one := big.NewInt(1)
	start := big.NewInt(0)
	for accts := new(big.Int).Set(start); accts.Cmp(totalAccounts) < 0; accts.Add(accts, one) {
		acct, err := acctsIndexInstance.Entry(&bind.CallOpts{}, accts)
		if err != nil {
			log.Fatal(err)
		}
		allAccounts = append(allAccounts, acct)
	}

	// instantiate the token contract
	tokenAddress := common.HexToAddress("0xb708175e3f6Cd850643aAF7B32212AFad50e2549")
	instance, err := token.NewToken(tokenAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	decimals, err := instance.Decimals(&bind.CallOpts{})
	if err != nil {
		log.Fatal(err)
	}

	// print list of all accounts with address key
	for _, a := range allAccounts {
		key, _ := toAddressKey(a.String(), ":cic.person")
		fmt.Println(a, key)
		url := fmt.Sprintf("http://localhost:63380/%v", key)
		resp, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		scanner := bufio.NewScanner(resp.Body)
		for i := 0; scanner.Scan() && i < 5; i++ {
			fmt.Println(scanner.Text())
		}

		if err := scanner.Err(); err != nil {
			panic(err)
		}

		// get balance of an address linked to contract
		address := common.HexToAddress(a.String())
		bal, err := instance.BalanceOf(&bind.CallOpts{}, address)
		if err != nil {
			log.Fatal(err)
		}

		fbal := new(big.Float)
		fbal.SetString(bal.String())
		// display token balance to 6 decimals
		value := new(big.Float).Quo(fbal, big.NewFloat(math.Pow10(int(decimals))))

		fmt.Printf("balance: %f\n", value)
	}
}
